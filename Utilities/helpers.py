import errno
import os
import shutil


def mkdir(path):
    if os.path.exists(path):
        return path
    else:
        try:
            os.mkdir(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
            print("Creation of the directory %s failed" % path)
            exit()
        else:
            print("Successfully created the directory %s " % path)
            return path


def mkdirs(path):
    if os.path.exists(path):
        return path
    else:
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
            print("Creation of the directory %s failed" % path)

        else:
            print("Successfully created the directory %s " % path)
            return path


def rmdir(path):
    try:
        os.rmdir(path)
    except OSError:
        print("Deletion of the directory %s failed" % path)
        exit()
    else:
        print("Successfully deleted the directory %s" % path)


def rmdirs(path):
    try:
        shutil.rmtree(path)
    except OSError:
        print("Deletion of the directory %s failed" % path)
        exit()
    else:
        print("Successfully deleted the directory %s" % path)

def end(message):
    print(message)
    exit()