from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os
import pickle
from copy import deepcopy

import cv2
import imutils
import numpy as np
import tensorflow as tf

import facenet.src.facenet as facenet
from face_detection import face_detection


def main():
    # set up required arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-cl", "--classifier",
                    help="path to .pkl file")
    ap.add_argument("-fn", "--facenet",
                    help="path to facenet .pb file")
    ap.add_argument("-c", "--confidence", type=float, default=0.80,
                    help="minimum probability to filter weak detections")
    ap.add_argument("-v", "--video", default="",
                    help="to load video file instead of camera source")
    ap.add_argument("-r", "--rotation", type=float, default=0,
                    help="rotation in degrees for input stream")
    ap.add_argument("-i", "--image", default="",
                    help="path to image rather than video")
    args = vars(ap.parse_args())

    INPUT_IMAGE_SIZE = 160

    os.environ['CUDA_VISIBLE_DEVICES'] = '1'
    # Load The Custom Classifier
    with open(args['classifier'], 'rb') as file:
        model, class_names = pickle.load(file)
        print(class_names)
        print("Custom Classifier, Successfully loaded")

    with tf.compat.v1.Graph().as_default():
        gpu_options = tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.6)
        sess = tf.compat.v1.Session(
            config=tf.compat.v1.ConfigProto(gpu_options=gpu_options, log_device_placement=False))

        with sess.as_default():
            # Load the model
            print('Loading feature extraction model')
            facenet.load_model(args['facenet'])
            # load face detection model
            network = face_detection.FaceDetection(args['confidence'])

            # Get input and output tensors
            images_placeholder = tf.compat.v1.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.compat.v1.get_default_graph().get_tensor_by_name("embeddings:0")
            phase_train_placeholder = tf.compat.v1.get_default_graph().get_tensor_by_name("phase_train:0")

            if args['video'] != '':
                cap = cv2.VideoCapture(args['video'])
            else:
                # use open cv to capture the video stream
                cap = cv2.VideoCapture(0)  # "http://10.1.2.230:8081")

            if args['image'] != "":
                frame1 = cv2.imread(args['image'])
            while True:

                if args['image'] != "":
                    frame = deepcopy(frame1)
                else:
                    _, frame = cap.read()

                frame = imutils.resize(frame, width=500)
                if args['rotation'] != 0:
                    frame = imutils.rotate(frame, 90)
                bounding_boxes = network.detect(frame, True)
                faces_found = len(bounding_boxes)
                if faces_found > 0:
                    for face in bounding_boxes:
                        (startX, startY, endX, endY) = face
                        cropped = frame[startY:endY, startX:endX]
                        scaled = cv2.resize(cropped, (INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE),
                                            interpolation=cv2.INTER_CUBIC)
                        scaled = facenet.prewhiten(scaled)
                        scaled_reshape = scaled.reshape(-1, INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE, 3)
                        feed_dict = {images_placeholder: scaled_reshape, phase_train_placeholder: False}
                        emb_array = sess.run(embeddings, feed_dict=feed_dict)
                        predictions = model.predict_proba(emb_array)

                        best_class_indices = np.argmax(predictions, axis=1)
                        best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]
                        best_name = class_names[best_class_indices[0]]

                        if best_class_probabilities >= args['confidence']:
                            name = best_name + " " + str(best_class_probabilities[0])
                        else:
                            name = "Unknown"

                        y = startY - 10 if startY - 10 > 10 else startY + 10
                        cv2.rectangle(frame, (startX, startY), (endX, endY),
                                      (0, 0, 255), 2)
                        cv2.putText(frame, name, (startX, y), cv2.FONT_HERSHEY_COMPLEX_SMALL,
                                    1, (255, 255, 255), thickness=1, lineType=2)

                    # print("Name: {}, Probability: {}".format(best_name, best_class_probabilities))

                cv2.imshow("Frame", frame)
                key = cv2.waitKey(1) & 0xFF

                # if the `q` key was pressed, break from the loop
                if key == ord("q"):
                    break
            cap.release()
            cv2.destroyAllWindows()


main()
