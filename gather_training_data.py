import argparse
import os
import time
import uuid

import cv2
import imutils
from PIL import Image

from Utilities import helpers
from face_detection import face_detection

# set up required arguments
ap = argparse.ArgumentParser()
ap.add_argument("-n", "--name", required=True,
                help="label for face")
ap.add_argument("-c", "--confidence", type=float, default=0.60,
                help="minimum probability to filter weak detections")
ap.add_argument("-s", "--seconds", type=int, default=2,
                help="number of seconds before the next detection")
ap.add_argument("-v", "--video", default="",
                help="to load video file instead of camera source")
ap.add_argument("-r", "--rotation", type=float, default=0,
                help="rotation in degrees for input stream")
ap.add_argument("-l", "--limit", type=int, default=50,
                help="maximum amount of images to gather")
args = vars(ap.parse_args())

# initialize the network with the trained model for face detection
print("[INFO] loading model...")
network = face_detection.FaceDetection(args['confidence'])

# initialize the video stream and allow the camera sensor to warm up
print("[INFO] starting video stream...")

time.sleep(2.0)
cap = None

if args['video'] != '':
    try:
        cap = cv2.VideoCapture(args['video'])
    except:
        helpers.end("Unable to open video source for image capturing.")

else:
    try:
        # use open cv to capture the video stream
        cap = cv2.VideoCapture(0)  # "http://10.1.2.230:8081"
    except:
        helpers.end("Unable to open video source for image capturing.")

# create a directory to store the images. (does not create if already existing)
helpers.mkdir(os.path.join(os.getcwd(), 'train_images/'))

# create directory based on label name.

image_dir_path = helpers.mkdir(os.path.join(os.path.join(os.getcwd(), "train_images/"), args['name']))
# initialize a counter to use with the image names
counter = 0

# for help tracking how much time has passed
previous = time.time()
delta = 0

net_input_pixels = (160, 160)
# loop over the frames from the video stream
while True:
    # calculate how much time has passed since last iter
    current = time.time()
    delta += current - previous
    previous = current

    # grab the frame from the threaded video stream and resize it
    _, frame = cap.read()
    if frame is None or frame == []:
        helpers.end("Video Frame empty. Exiting.")

    frame = imutils.resize(frame, width=800)
    if args['rotation'] != 0:
        frame = imutils.rotate(frame, 90)

    face = None
    try:
        # feed frame to detect function
        face = network.detect(frame=frame, multiple=False)  # only detects a single face (the one in the foreground).
    except:
        helpers.end('Unable to load facial detection network.')

    if len(face) > 0:
        # extract the co-ordinates of the detected face into a tuple
        (startX, startY, endX, endY) = face
        # extract just the face portion of image
        cropped_face = frame[startY:endY, startX:endX]

        # only write image if amount of time user specified has passed
        if delta > args["seconds"]:
            # write the cropped face to disk
            # resize pixels to the model size
            cropped_face = cv2.cvtColor(cropped_face, cv2.COLOR_BGR2RGB)
            cropped_face = Image.fromarray(cropped_face)
            cropped_face = cropped_face.resize(net_input_pixels)
            cropped_face.save(os.path.join(image_dir_path, args['name'] + '_' + str(uuid.uuid1()) + '.jpg'))
            # cropped_face = imutils.resize(cropped_face, width=128, height=128)
            # cv2.imwrite(os.path.join(image_dir_path, args['name'] + '_' + str(uuid.uuid1()) + '.jpg'), cropped_face)
            print('Image written')
            delta = 0
            # increment counter
            counter += 1

        y = startY - 10 if startY - 10 > 10 else startY + 10
        # add bounsding rectangle to face on original frame
        cv2.rectangle(frame, (startX, startY), (endX, endY),
                      (0, 0, 255), 2)
        cv2.putText(frame, "taken: " + str(counter), (startX, y), cv2.FONT_HERSHEY_COMPLEX_SMALL,
                    1, (255, 255, 255), thickness=1, lineType=2)

    if counter >= int(args['limit']):
        break
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF

    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break

cap.release()
cv2.destroyAllWindows()

if not os.listdir(image_dir_path):
    os.remove(image_dir_path)
