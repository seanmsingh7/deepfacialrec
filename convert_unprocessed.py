import argparse
import os
import time
import uuid

import cv2
from PIL import Image

from face_detection import face_detection

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--directory", required=True,
                help="directory with unprocessed images")

args = vars(ap.parse_args())

# initialize the network with the trained model for face detection
print("[INFO] loading model...")
network = face_detection.FaceDetection()

# initialize the video stream and allow the camera sensor to warm up
print("[INFO] starting video stream...")

time.sleep(2.0)
net_input_pixels = (160, 160)
image_dir_path = os.path.join(os.getcwd(), 'train_images')
for dir in os.listdir(args['directory']):
    class_path = os.path.join(args['directory'], dir)
    for image in os.listdir(class_path):
        image_path = os.path.join(class_path, image)
        frame = cv2.imread(image_path)
        # frame = imutils.resize(frame, width=800)
        face = network.detect(frame=frame, multiple=False)  # only detects a single face (the one in the foreground).
        if len(face) > 0:
            if not os.path.exists(os.path.join(image_dir_path, dir)):
                os.mkdir(os.path.join(image_dir_path, dir))

            # extract the co-ordinates of the detected face into a tuple
            (startX, startY, endX, endY) = face
            # extract just the face portion of image
            cropped_face = frame[startY:endY, startX:endX]
            # write the cropped face to disk
            # resize pixels to the model size
            try:
                cropped_face = cv2.cvtColor(cropped_face, cv2.COLOR_BGR2RGB)
            except cv2.error:
                continue
            cropped_face = Image.fromarray(cropped_face)
            cropped_face = cropped_face.resize(net_input_pixels)
            cropped_face.save(os.path.join(os.path.join(image_dir_path, dir), dir + '_' + str(uuid.uuid1()) + '.jpg'))
            # cropped_face = imutils.resize(cropped_face, width=128, height=128)
            # cv2.imwrite(os.path.join(image_dir_path, args['name'] + '_' + str(uuid.uuid1()) + '.jpg'), cropped_face)
            print('Image written')
