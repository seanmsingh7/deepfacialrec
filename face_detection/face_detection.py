import cv2
import numpy as np


class FaceDetection:
    def __init__(self, confidence=0.50):
        self.model = "models/face_detection.caffemodel"
        self.prototxt = "models/face_detection_deploy.prototxt"
        print('Loading caffe face detection network')
        self.net = cv2.dnn.readNetFromCaffe(prototxt=self.prototxt, caffeModel=self.model)
        self.confidence = confidence

    def detect(self, frame, multiple=True):
        all_detections = []
        (h, w) = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0,
                                     (300, 300), (104.0, 177.0, 123.0))
        self.net.setInput(blob)
        detections = self.net.forward()
        for i in range(0, detections.shape[2]):
            # extract the confidence (i.e., probability) associated with the
            # prediction
            confidence = detections[0, 0, i, 2]

            # filter out weak detections by ensuring the `confidence` is
            # greater than the minimum confidence
            if confidence < self.confidence:
                continue

            # compute the (x, y)-coordinates of the bounding box for the
            # object

            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            # (startX, startY, endX, endY) = box.astype("int")
            all_detections.append(box.astype("int"))

        if multiple:
            return all_detections
        else:
            return self.detectLargestBoundingBox(all_detections)

    def detectLargestBoundingBox(self, detections):
        largest = 0
        largest_box = []
        for detection in detections:
            box_perimeter = self.__calculate_perimeter_of_box(detection)
            if box_perimeter > largest:
                largest = box_perimeter
                largest_box = detection
        return largest_box

    def __calculate_perimeter_of_box(self, box):
        (startX, startY, endX, endY) = box
        width = endX - startX
        length = endY - startY
        return 2 * (width + length)
