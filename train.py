import os
from subprocess import call


class Trainer:
    def __init__(self):
        x_train = []
        y_train = []

    def train(self, train_dataset):
        mode = 'TRAIN'
        data_dir = train_dataset
        model = os.path.join(os.getcwd(), 'models/facenet/20180402-114759.pb')
        classifier_filename = os.path.join(os.getcwd(), 'models/classifier_file.pkl')
        use_split_dataset = "false"
        min_nrof_images_per_class = '10'

        if os.path.exists(classifier_filename) and mode == 'TRAIN':
            os.remove(classifier_filename)
        call(["python",
              'facenet/src/classifier.py',
              mode,
              data_dir,
              model,
              classifier_filename,
              '--min_nrof_images_per_class', min_nrof_images_per_class
              ])


trainer = Trainer()
train_dataset = os.path.join(os.getcwd(), 'train_images')
trainer.train(train_dataset=train_dataset)
